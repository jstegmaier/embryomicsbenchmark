/**
 * Generating Semi-Synthetic Validation Benchmarks for Embryomics.
 * Copyright (C) 2016
 * J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Strähle, R. Sanders and R. Mikut
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Strähle, R. Sanders and R. Mikut 
 * Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.
 *
 */

------------------------------------------------------------------------------------------
INSTALLATION INSTRUCTIONS:
------------------------------------------------------------------------------------------
Toolbox for generating benchmark images based on real cell/embryo data

------------------------------------------------------------------------------------------
1. Prerequisites:
------------------------------------------------------------------------------------------
1.1 Clone the repository using the following command "git clone git@bitbucket.org:jstegmaier/embryomicsbenchmark.git myrepositoryfolder". The repository contains all relevant MATLAB code as well as the video snippet library to generate 2D and 3D images.

1.2 The toolbox requires a matlab file as input which contains the following variables:
- d_orgs: 3D matrix with dimensions N x K x F (1. Dim: data tuples, 2. Dim: Time, 3. Dim: Features)
- here, N corresponds to the maximum number of objects/cells observed at a single frame in the experiment
- if a time point has less than N objects, the corresponding excess rows are set to zero
- The benchmark requires spatial locations (3. Dimension in d_orgs, feature indices 3-5) 
- Additionally, it expects successfully tracked objects to have a non-zero tracking state id.
- If objects were not successfully tracked, this id should be set to zero and the corresponding object will be ignored by the benchmark
- Furthermore, a density value for each of the objects is required, which can e.g. simply be the number of objects in a radius r around the object.
- You find an exemplary project file containing tracked nuclei of a zebrafish embryo in the download section of the repository (https://bitbucket.org/jstegmaier/embryomicsbenchmark/downloads/ExampleDataZebrafish.zip)

1.3 If you want to generate artificially flawed images, you additionally need the XPIWIT application which can be obtained from https://bitbucket.org/jstegmaier/xpiwit/downloads/. Further details on the usage of XPIWIT can be found in the respective documentation and in the quick start guide manual.


------------------------------------------------------------------------------------------
2. Executing the Software to Generate a Simulated Embryo and Associated Raw Images
------------------------------------------------------------------------------------------
2.1 After having a valid project file available, you can start the main script of the benchmark generation, namely the file "GenerateBenchmark.m" via MATLAB. This opens a file selection dialog, where you have to point the script to the project file containing real cell locations and tracking information

2.2 If a valid project file was selected the benchmark options dialog opens, which allows to setup the simulation parameters, such as number of cells, division cycle lengths, image generation and the like. For more details on the individual parameter settings refer to the corresponding publication mentioned below.

2.3 After the project settings were specified, the benchmark as well as the associated images are being generated. This can take a few minutes up to a few hours, depending on the complexity of the benchmark and the performance of your computer.


------------------------------------------------------------------------------------------
3. Acquisition Simulation
------------------------------------------------------------------------------------------
3.1 The generated benchmark images do not contain any flaws like noise, illumination variation and the like. To apply the acquisition simulation, we prepared an XPIWIT XML pipeline that can be found in the Software/Data/ folder of the repository. Simply open the pipeline in the graphical user interface of XPIWIT, drag and drop your input images as well as the point spread function file "Angle0.tif" to the input edit fields and press apply to perform the noise generation. The result images including the specified acquisition deficiencies are placed in the output folder.


------------------------------------------------------------------------------------------
4. Citing the Benchmark
------------------------------------------------------------------------------------------
4.1 If you use this application for your scientific work, please cite the following publications:
 
J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Strähle, R. Sanders and R. Mikut: Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.

A. Bartschat, E. Hübner, M. Reischl, R. Mikut and J. Stegmaier: XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit, Bioinformatics, 2015.