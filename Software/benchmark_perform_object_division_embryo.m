%%%
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics.
%% Copyright (C) 2016
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% Please refer to the documentation for more information about the software
%% as well as for installation instructions.
%%
%% If you use this application for your scientific work, please cite the following publication
%%
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut 
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.
%%
%%%
function [currentObject, nextObject1, nextObject2] = benchmark_perform_object_division_embryo(currentObject, newObjectId, objectVideos, settings)

indices = settings.indices;

%% clone the current object for the next frame and alter its variables
nextObject1 = currentObject;
nextObject2 = currentObject;

currentObjectIndex = currentObject(indices.id);
currentObject(indices.successorIDs) = [currentObjectIndex, newObjectId];
nextObject1(indices.predecessorID) = currentObjectIndex;
nextObject2(indices.predecessorID) = currentObjectIndex;
nextObject2(indices.id) = newObjectId;

%% select random object video for the new objects
objectVideoIDs = randperm(settings.numObjectVideos);
nextObject1(indices.objectVideoId) = objectVideoIDs(1);
nextObject2(indices.objectVideoId) = objectVideoIDs(2);

%% reset the division cycle state
nextObject1(indices.divisionCycleState) = 1;
nextObject2(indices.divisionCycleState) = 1;
nextObject1(indices.divisionCycleLength) = settings.divisionCycleMinLength + randperm(settings.divisionCycleMaxLength-settings.divisionCycleMinLength, 1);
nextObject2(indices.divisionCycleLength) = settings.divisionCycleMinLength + randperm(settings.divisionCycleMaxLength-settings.divisionCycleMinLength, 1);

%% alter the positions, such that not both nuclei have the exact same position
objectVideoID = currentObject(indices.objectVideoId);
%divisionCycleState = currentObject(indices.divisionCycleState);

%% get the current mask
lastMaskIndex = length(objectVideos{objectVideoID}.objects);
currentMask = objectVideos{objectVideoID}.objects{lastMaskIndex}.groundTruthImage;
%imageDimension = ndims(currentMask);
currentMaskSize = size(currentMask) .* settings.groundTruthSpacing(1:ndims(currentMask));

%% find non-zero indices of the mask
[x,y,z,~] = find3(imresize3(currentMask, currentMaskSize) > 0);

%% de-mean the data (only neccessary for visualization)
data = [x,y,z]-ones(size(x,1),1)*mean([x,y,z],1);

if (settings.images2D == true)
    data(:,3) = 0;
end

%% perform principal component analysis to determine the major axis
[coeff, ~] = princomp(data);

%% show debug figure if enabled
debug_figures = false;
if (debug_figures == true)
    figure; hold on;
    plot3(data(:,1),data(:,2),data(:,3), 'ob');
    xlabel('x');
    ylabel('y');
    zlabel('z');

    quiver3(0,0,0,coeff(1,1),coeff(2,1),coeff(3,1), '-r');
    quiver3(0,0,0,coeff(1,2),coeff(2,2),coeff(3,2), '-g');
    quiver3(0,0,0,coeff(1,3),coeff(2,3),coeff(3,3), '-m');
end

%objectVideoID = nextObject1(indices.objectVideoId);
nextObject1(indices.radius) = settings.minRadius + rand(1)*(settings.maxRadius - settings.minRadius); %1 * max(size(objectVideos{objectVideoID}.objects{1}.groundTruthImage) .* settings.groundTruthSpacing(1:imageDimension));
%objectVideoID = nextObject2(indices.objectVideoId);
nextObject2(indices.radius) = settings.minRadius + rand(1)*(settings.maxRadius - settings.minRadius); %1 * max(size(objectVideos{objectVideoID}.objects{1}.groundTruthImage) .* settings.groundTruthSpacing(1:imageDimension));

%% set the position according to the major axis of the current object, i.e., the correct division direction
nextObject1(indices.position) = currentObject(indices.position) + 0.25*(coeff(:,1)*nextObject1(indices.radius));
nextObject2(indices.position) = currentObject(indices.position) - 0.25*(coeff(:,1)*nextObject2(indices.radius));

if (settings.images2D == true)
    currentObject(indices.position(3)) = 0;
    nextObject1(indices.position(3)) = 0;
    nextObject2(indices.position(3)) = 0;
end

if (debug_figures == true)
    figure; hold on;
    xlabel('x');
    ylabel('y');
    zlabel('z');

    plot3(objectStruct(currentObject, currentTimePoint).position(1),objectStruct(currentObject, currentTimePoint).position(2),objectStruct(currentObject, currentTimePoint).position(3),'*r');
    plot3(objectStruct(currentObject, currentTimePoint+1).position(1),objectStruct(currentObject, currentTimePoint+1).position(2),objectStruct(currentObject, currentTimePoint+1).position(3),'*g');
    plot3(objectStruct(newObjectId, currentTimePoint+1).position(1),objectStruct(newObjectId, currentTimePoint+1).position(2),objectStruct(newObjectId, currentTimePoint+1).position(3),'*b');
end

%objectStruct(currentObject, currentTimePoint+1).position = objectStruct(currentObject, currentTimePoint+1).position + [rand,rand,rand]*objectStruct(currentObject, currentTimePoint+1).maxVelocity;
%objectStruct(newObjectId, currentTimePoint+1).position = objectStruct(newObjectId, currentTimePoint+1).position + [rand,rand,rand]*objectStruct(newObjectId, currentTimePoint+1).maxVelocity;
end