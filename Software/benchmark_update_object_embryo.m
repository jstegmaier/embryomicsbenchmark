%%%
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics.
%% Copyright (C) 2016
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% Please refer to the documentation for more information about the software
%% as well as for installation instructions.
%%
%% If you use this application for your scientific work, please cite the following publication
%%
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut 
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.
%%
%%%
function [currentObject, nextObject] = benchmark_update_object_embryo(currentObject, kdtree, directions, kdtreeComplete, kdtreeSimulated, d_orgs, currentTime, settings)

%% get the indices
indices = settings.indices;

%% clone the current object for the next frame and alter its variables
currentObjectIndex = currentObject(indices.id);
nextObject = currentObject;
nextObject(indices.predecessorID) = currentObjectIndex;

%% update successor and predecessor information for the current object
currentObject(indices.successorIDs) = [currentObjectIndex, 0];

%% update the division cycle state
nextObject(indices.divisionCycleState) = min(nextObject(indices.divisionCycleState) + settings.timeStep, nextObject(indices.divisionCycleLength));

%% possible states (not implemented yet)
%% - move      0
%% - hide      1
%% - divide    2
%nextObject(indices.state) = 0;

%% activate or deactivate the forces
repulsiveForceActive = true;
adhesiveForceActive = false;
neighborhoodCentroidForceActive = true;
neighborhoodMovementForceActive = true;

%% initialize all forces to zero
averageDisplacement = zeros(1,3); 
repulsiveForce = zeros(1,3);
adhesiveForce = zeros(1,3);
neighborhoodDirection = zeros(1,3);

%% update the position of the object
if (neighborhoodMovementForceActive == true)
    numNeighbors = 10;
    [neighborIndices,neighborDistances] = knnsearch(kdtree, currentObject(indices.position)', 'K', numNeighbors);
    averageDisplacement = mean(directions(neighborIndices,:), 1);
end

if (neighborhoodCentroidForceActive == true)
    numNeighbors = 1;
    [neighborIndices,neighborDistances] = knnsearch(kdtreeComplete, currentObject(indices.position)', 'K', numNeighbors);
    neighborhoodCentroid = mean(kdtreeComplete.X(neighborIndices,:), 1);
    neighborhoodDirection = 0.1 * (neighborhoodCentroid - currentObject(indices.position)');

    if (norm(neighborhoodDirection) > norm(averageDisplacement))
       neighborhoodDirection = neighborhoodDirection / norm(neighborhoodDirection) * norm(averageDisplacement); 
    end
end

%% calculate the distances to all other nuclei in order to determine their influence
if (adhesiveForceActive == true || repulsiveForceActive == true)
%     validIndices = find(squeeze(d_orgs(:,currentTime,indices.id)) > 0);
%     validIndices(validIndices==currentObjectIndex) = [];


    [validIndices,~] = rangesearch(kdtreeSimulated, currentObject(indices.position)', settings.maxRadius*5);
    validIndices = validIndices{1}(2:end)';
    distances = [d_orgs(validIndices,currentTime,3) - currentObject(indices.position(1)), ...
                 d_orgs(validIndices,currentTime,4) - currentObject(indices.position(2)), ...
                 d_orgs(validIndices,currentTime,5) - currentObject(indices.position(3))];
end

%% calculate the repulsive force
if (repulsiveForceActive == true && ~isempty(neighborDistances))
    nucleusRadii = d_orgs(validIndices, currentTime, indices.radius) + currentObject(indices.radius);
    membraneRadii = 2*nucleusRadii;
    repulsiveForce = sum(benchmark_calculate_repulsive_force(distances, nucleusRadii, membraneRadii, 1, 1));
end

%% calculate the adhesive force
if (adhesiveForceActive == true && ~isempty(neighborDistances))
    outerRadii = membraneRadii+4;
    adhesiveForce = sum(benchmark_calculate_adhesive_force(distances, outerRadii, 1));
end

%% update the next object position
nextObject(indices.position) = generate_rowvector(nextObject(indices.position)) + averageDisplacement + repulsiveForce + 0.5184*adhesiveForce + neighborhoodDirection;

%% reset third dimension to zero
if (settings.images2D == true)
    nextObject(indices.position(3)) = 0;
end
end