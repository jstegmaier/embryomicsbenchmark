%%%
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics.
%% Copyright (C) 2016
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% Please refer to the documentation for more information about the software
%% as well as for installation instructions.
%%
%% If you use this application for your scientific work, please cite the following publication
%%
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut 
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.
%%
%%%
function d_orgs = PerformSimulation(d_orgs, kdtree, directions, kdtreeComplete, objectVideos, settings)

    indices = settings.indices;
    for i=1:(settings.numTimePoints-1)

        %% update the objects
        objectIndices = find(squeeze(d_orgs(:,i,1))>0);
        numObjects = max(size(objectIndices));
        nextObjectIndex = numObjects+1;
        
        %% calculate percentual density
        kdtreeSimulated = KDTreeSearcher(squeeze(d_orgs(objectIndices,i,3:5)), 'distance','euclidean');
        for j=generate_rowvector(objectIndices)
            neighborIndicesSimulated = rangesearch(kdtreeSimulated, squeeze(d_orgs(j,i,indices.position))', 40);
            neighborIndicesReal = rangesearch(kdtree{i}, squeeze(d_orgs(j,i,indices.position))', 40);
            currentDensitySimulated = length(neighborIndicesSimulated{1}) / numObjects;
            currentDensityReal = length(neighborIndicesReal{1}) / size(kdtree{i}.X,1);
            d_orgs(j,i,indices.density) = length(neighborIndicesSimulated{1});
            d_orgs(j,i,indices.relativeDensity) = currentDensitySimulated;
            d_orgs(j,i,indices.densityDifference) = currentDensityReal - currentDensitySimulated; 
        end
        
        if (settings.percentageMode == true)
            %% calculate the number of divisions necessary
            numDivisions = round(max(0, (settings.nucleiPercentage * size(kdtree{i}.X,1) - numObjects)));
            
            %%
            useDivisionCycleState = false;
            if (useDivisionCycleState == true)
                divisionCycleStates = generate_columnvector(squeeze(d_orgs(objectIndices,i,indices.divisionCycleState)));
                [~, divisionIndices] = sortrows(divisionCycleStates,-1);
                divisionIndices = divisionIndices(1:numDivisions);
            else
                densityDifferences = squeeze(d_orgs(objectIndices,i,[indices.densityDifference, indices.divisionCycleState]));
                densityDifferences(:,1) = round(densityDifferences(:,1) * 10000) / 10000;
                [densityDifferences, divisionIndices] = sortrows(densityDifferences,[-1, -2]);
                validDivisions = find(densityDifferences(:,2)>=settings.divisionCycleMinLength);
                
                if (length(validDivisions) >= numDivisions)
                    divisionIndices = divisionIndices(validDivisions(1:numDivisions));
                else
                    divisionIndices = divisionIndices(1:numDivisions);
                end
            end
        end
        
        for j=generate_rowvector(objectIndices)

            currentObject = squeeze(d_orgs(j,i,:));
            
            if (settings.percentageMode == true)
                performDivision = sum(ismember(divisionIndices, j));
            end
            
            %% check if cell division cycle has ended
            %if (currentObject(indices.divisionCycleState) >= currentObject(indices.divisionCycleLength) && numObjects < settings.numMaxObjects)
            if ((settings.percentageMode == true && performDivision == true) || ...
                (settings.percentageMode == false && currentObject(indices.divisionCycleState) >= currentObject(indices.divisionCycleLength) && numObjects < settings.numMaxObjects))
                %% perform division for the current object
                [currentObject, nextObject1, nextObject2] = benchmark_perform_object_division_embryo(currentObject, nextObjectIndex, objectVideos, settings);
                d_orgs(j,i,:) = currentObject;
                d_orgs(j,i+1,:) = nextObject1;
                d_orgs(nextObjectIndex,i+1,:) = nextObject2;
                nextObjectIndex = nextObjectIndex+1;
            else
%                 if (d_orgs(j,i,indices.divisionCycleState) >= d_orgs(j,i,indices.divisionCycleLength) && numObjects >= settings.numMaxObjects)
%                     d_orgs(j,i,indices.divisionCycleState) = 1; %% check if it makes sense to reset this (probably not as the size changes)
%                 end

                %% update the current object division cycle has not ended yet
                [currentObject, nextObject] = benchmark_update_object_embryo(currentObject, kdtree{i}, directions{i}, kdtreeComplete{i}, kdtreeSimulated, d_orgs, i, settings);
                d_orgs(j,i,:) = currentObject;
                d_orgs(j,i+1,:) = nextObject;
            end
        end
        
        if (settings.images2D == true)
            d_orgs(:,i,indices.position(3)) = 0;
        end

        %% print status
        if (mod(i,11) == 0)
            fprintf( '%f%% processed ... \n', 100*i/settings.numTimePoints)
        end
    end
end