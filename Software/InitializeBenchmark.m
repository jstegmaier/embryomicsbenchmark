%%%
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics.
%% Copyright (C) 2016
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% Please refer to the documentation for more information about the software
%% as well as for installation instructions.
%%
%% If you use this application for your scientific work, please cite the following publication
%%
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut 
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.
%%
%%%
function [d_orgs, kdtree, directions, kdtreeComplete] = InitializeBenchmark(d_orgs_embryo, ~, settings)
    
    indices = settings.indices;

    %% iterate over all tracklets and try to find a matching partner
    disp( ['Creating kd-tree for ' num2str(size(d_orgs_embryo,2)) ' time points ...'] );
    kdtree = cell(1,(size(d_orgs_embryo,2)-1));
    kdtreeComplete = cell(1,(size(d_orgs_embryo,2)-1));
    directions = cell(1,(size(d_orgs_embryo,2)-1));
    for i=1:(size(d_orgs_embryo,2)-1)
        
        %% create kd-tree for the hyper positions (xyzt)
        validIndices = d_orgs_embryo(:,i,1) > 0;
        minQuantileDensity = quantile(d_orgs_embryo(validIndices,i,6), settings.quantileThreshold);
        kdtreeComplete{i} = KDTreeSearcher(squeeze(d_orgs_embryo(validIndices,i,3:5)), 'distance','euclidean');
        
        %% fill the kdtree that only uses tracked nuclei
        if (settings.revertTime == false)
            validIndices = d_orgs_embryo(:,i,1) > 0 & d_orgs_embryo(:,i,6) > minQuantileDensity & d_orgs_embryo(:,i,7) > 0;
        else
            validIndices = d_orgs_embryo(:,i,1) > 0 & d_orgs_embryo(:,i,6) > minQuantileDensity & d_orgs_embryo(:,i+1,7) > 0;
        end
        kdtree{i} = KDTreeSearcher(squeeze(d_orgs_embryo(validIndices,i,3:5)), 'distance','euclidean');
        directions{i} = squeeze(d_orgs_embryo(validIndices,i+1,3:5)) - squeeze(d_orgs_embryo(validIndices,i,3:5));
        
        %% disable direction in z for 2d images
        if (settings.images2D == true)
           directions{i}(:,3) = 0; 
        end
                
        %% plot progress
        if (mod(i,100) == 0)
            fprintf( '%f%% processed ...\n', 100*i/size(d_orgs_embryo,2));
        end
    end

    selectedIndices = randperm(size(kdtree{1}.X,1), min(size(kdtree{1}.X,1), settings.numInitialObjects));
    d_orgs = zeros(settings.numMaxObjects, settings.numTimePoints, 11);
    for i=1:length(selectedIndices)

        %% initialize physical position in the simulated sample space
        d_orgs(i,1,indices.id) = i;
        d_orgs(i,1,indices.position) = kdtree{1}.X(selectedIndices(i),:);
        
        %% disable direction in z for 2d images
        if (settings.images2D == true)
           d_orgs(i,1,indices.position(3)) = 0; 
        end

        %% random initialization for the state of the cell
        d_orgs(i,1,indices.divisionCycleLength) = settings.divisionCycleMinLength + randperm(settings.divisionCycleMaxLength-settings.divisionCycleMinLength, 1);
        if (settings.startSynchronized == true)
            d_orgs(i,1,indices.divisionCycleState) = randperm(d_orgs(i,1,indices.divisionCycleLength), 1);
        else
            d_orgs(i,1,indices.divisionCycleState) = 1;
        end
        
        %% setup the division state, object video and division cycle length
        d_orgs(i,1,indices.objectVideoId) = randperm(settings.numObjectVideos, 1);
        d_orgs(i,1,indices.radius) = settings.minRadius + rand(1)*(settings.maxRadius - settings.minRadius); %1 * max(size(objectVideos{d_orgs(i,1,indices.objectVideoId)}.objects{1}.groundTruthImage) .* generate_rowvector(settings.groundTruthSpacing(1:settings.imageDimension)));
        
        %% setup progenitors and descendants
        d_orgs(i,1,indices.relativeDensity) = 0;
        d_orgs(i,1,indices.densityDifference) = 0;
        d_orgs(i,1,indices.predecessorID) = 0;
        d_orgs(i,1,indices.successorIDs) = [0, 0];
    end
end