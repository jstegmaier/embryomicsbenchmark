function [ columnvector ] = generate_columnvector( vector )
%function [ columnvector ] = generate_columnvector( vector )
%returns the transposed vector if vector is a column vector and vector else

if iscolumn(vector)
   columnvector = vector;
else
   columnvector = vector';
end

