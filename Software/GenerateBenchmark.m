%%%
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics.
%% Copyright (C) 2016
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% Please refer to the documentation for more information about the software
%% as well as for installation instructions.
%%
%% If you use this application for your scientific work, please cite the following publication
%%
%% J. Stegmaier, J. Arz, B. Schott, M. Takamiya, J. C. Otte, A. Kobitski, G. U. Nienhaus, U. Str?hle, R. Sanders and R. Mikut 
%% Generating Semi-Synthetic Validation Benchmarks for Embryomics. 2016.
%%
%%%

%% add the path to the bioformats scripts
addpath('BioFormats/');

%% start timer
tic;

%% open the desired project file
[fileName, pathName] = uigetfile('*.*', 'Select a valid *.mat or *.prjz (GaitCAD project) file containing tracked cell locations, density and tracking state variables (see documentation for details).', '../Data/');
embryoProjectFile = [pathName fileName];

%% read user defined project settings
projectSettings = inputdlg({'Image Size (x,y,z):', ...
                            'Input Spacing (x,y,z):', ...
                            'Nuclei Percentage (p in [0,1]):', ...
                            'Time Range (t_min, t_max):', ...
                            'Object Radii (r_min, r_max):', ...
                            'Division Cycle Lenght (l_min, l_max):', ...
                            'Generate Images?', ...
                            'Maximum Projection Images?', ...
                            'Output Spacing (x,y,z):'}, ...
                            'Project Settings', [1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50], {'2560, 2160, 2500', '0.4, 0.4, 0.4', '0.1', '100, 599', '7, 10', '28, 46', '0', '0', '0.8, 0.8, 2.0'});
                            %'Project Settings', [1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50], {'1298, 1298, 1250', '0.4, 0.4, 0.4', '1.0', '1, 400', '7, 10', '28, 46', '1', '0.8, 0.8, 2.0'});

generateData = true;
generateImages = str2num(projectSettings{7});
debugFigures = true;

%% indices used for the generated data structure (DO NOT CHANGE!)
indices.id = 1;
indices.radius = 2;
indices.position = 3:5;
indices.divisionCycleState = 6;
indices.divisionCycleLength = 7;
indices.density = 8;
indices.relativeDensity = 9;
indices.densityDifference = 10;
indices.objectVideoId = 11;
indices.predecessorID = 12;
indices.successorIDs = 13:14;
indices.relativeDivisionCycleState = 15;
var_bez = char('id', 'radius', 'xpos', 'ypos', 'zpos', 'divisionCycleState', 'divisionCycleLength', 'density', 'relativeDensity', 'densityDifference', 'objectVideoId', 'predecessorID', 'successorID1', 'successorID2');


% settings.images2D = false;
% if (settings.images2D == false)
settings.imageSize = str2num(projectSettings{1});
settings.imageSpacing = str2num(projectSettings{2});
settings.quantileThreshold = 0.05;
settings.nucleiPercentage = str2num(projectSettings{3});
% else
%     settings.imageSize = [1100, 700, 1];
%     settings.imageSpacing = [0.3,0.3,0];
%     settings.quantileThreshold = 0.0;
%     settings.nucleiPercentage = 1.0;
% end
settings.maximumProjectionImages = str2num(projectSettings{8});
settings.outputImageSpacing = str2num(projectSettings{9});
settings.groundTruthSpacing = [0.25, 0.25, 0.4];
settings.physicalImageSize = settings.imageSize .* settings.imageSpacing;

settings.indices = indices;
settings.percentageMode = true;
timeRange = str2num(projectSettings{4});
settings.startTime = timeRange(1);
settings.numTimePoints = timeRange(2)-timeRange(1)+1;
radiusRange = str2num(projectSettings{5});
settings.minRadius = radiusRange(1);
settings.maxRadius = radiusRange(2);
divisionCycleRange = str2num(projectSettings{6});
settings.divisionCycleMinLength = divisionCycleRange(1);
settings.divisionCycleMaxLength = divisionCycleRange(2);

%% unused parameters
settings.images2D = false;
settings.timeStep = 1;
settings.numInitialObjects = 60;
settings.numMaxObjects = 2000;
settings.startSynchronized = false;
settings.revertTime = false;

%% path to the object videos on which the benchmark is based
if (settings.images2D == true)
    settings.objectVideosFile = ['..' filesep 'Data' filesep 'ObjectVideoLibraries' filesep 'objectVideos2D.mat'];
else
    settings.objectVideosFile = ['..' filesep 'Data' filesep 'ObjectVideoLibraries' filesep 'objectVideos3D.mat'];
end

disp( 'Loading object videos for ground truth generation ...' );
if (~exist(settings.objectVideosFile, 'file'))
    disp( 'Error loading object video file: The file does not exist!' );
elseif (exist('objectVideos', 'var') && (ndims(objectVideos{1}.objects{1}.groundTruthImage) == 3 && settings.images2D == false))
    disp( 'Object videos already loaded, using loaded data ...' );
else
    load(settings.objectVideosFile);
end
settings.imageDimension = ndims(objectVideos{1}.objects{1}.groundTruthImage);
settings.numObjectVideos = length(objectVideos);        %% number of different sub-videos that are used as object basis

if (generateData == true)
    %% load the embryo data and scale it to physical units
    if (exist(embryoProjectFile, 'file'))
        disp('Loading embryo data ...');
        load(embryoProjectFile, '-mat');
        d_orgs_embryo = d_orgs;
        d_orgs_embryo(:,:,3) = d_orgs_embryo(:,:,3) * settings.imageSpacing(1);
        d_orgs_embryo(:,:,4) = d_orgs_embryo(:,:,4) * settings.imageSpacing(2);
        d_orgs_embryo(:,:,5) = d_orgs_embryo(:,:,5) * settings.imageSpacing(3);

        indicesEmbryo.id = 1;
        indicesEmbryo.volume = 2;
        indicesEmbryo.position = 3:5;

%         if (settings.images2D == false)
%             indicesEmbryo.density = 11;
%             indicesEmbryo.trackingState = 12;
%         else
            indicesEmbryo.density = 11; % set this to 7 for the neural crest project
            indicesEmbryo.trackingState = 12; % set this to 6 for the neural crest project
%         end
        selectedIndices = [indicesEmbryo.id, indicesEmbryo.volume, indicesEmbryo.position, indicesEmbryo.density, indicesEmbryo.trackingState];
        
        if (settings.revertTime == false)
            d_orgs_embryo = d_orgs_embryo(:,settings.startTime:(settings.startTime+settings.numTimePoints-1), selectedIndices);
        else
            d_orgs_embryo = d_orgs_embryo(:,(settings.startTime+settings.numTimePoints-1):-1:settings.startTime, selectedIndices);
        end
    else
        disp( 'Error loading embryo data file: The file does not exist!' );
    end

    if (settings.percentageMode == true)
        settings.numInitialObjects = round(settings.nucleiPercentage * sum(d_orgs_embryo(:,1,7)>0));
        settings.numMaxObjects = round(settings.nucleiPercentage * sum(d_orgs_embryo(:,end,7)>0));
    end

    %% initialize the benchmark
    disp('Initializing simulated embryo ...');
    [d_orgs, kdtree, directions, kdtreeComplete] = InitializeBenchmark(d_orgs_embryo, objectVideos, settings);
    if (debugFigures == true)
       validIndices = d_orgs(:,1,1) > 0;
       sum(validIndices)
       figure; plot3(d_orgs(validIndices,1,3), d_orgs(validIndices,1,4), d_orgs(validIndices,1,5), '.r');
       axis equal;
       view(0,-90);
    end

    %% perform the simulation for the desired number of frames
    disp('Performing embryo simulation ...');
    d_orgs = PerformSimulation(d_orgs, kdtree, directions, kdtreeComplete, objectVideos, settings);
    toc;
    
    if (debugFigures == true)
       figure; 
       physicalImageSize = settings.imageSize.*settings.imageSpacing;
       for i=1:size(d_orgs,2)
           %subplot(1,2,1);
           validIndices = d_orgs(:,i,1) > 0;
           sum(validIndices)
           plot3(d_orgs(validIndices,i,3), d_orgs(validIndices,i,4), d_orgs(validIndices,i,5), '.r');
           axis equal;
           axis([0, physicalImageSize(1), 0, physicalImageSize(2), 0, physicalImageSize(3)]);
           title(num2str(i)); hold on;
           view(0,-90)

    %        %       subplot(1,2,2);
    %        validIndices = d_orgs_embryo(:,i,end) > 0;
    %        plot3(d_orgs_embryo(validIndices,i,3), d_orgs_embryo(validIndices,i,4), d_orgs_embryo(validIndices,i,5), 'og');
    %        axis equal;
    %        axis([0, physicalImageSize(1), 0, physicalImageSize(2), 0, physicalImageSize(3)]);
    %        title(num2str(i));
            hold off;
           pause(0.05);
       end
    end
end

%% identify the split time points to calculate the video frame
d_orgs(:,:,end+1) = 0;
for i=1:size(d_orgs,1)
   currentObject = squeeze(d_orgs(i,:,:));
   divisionCycleState = currentObject(:,indices.divisionCycleState);
   lastDivisionTimePoint = 1;
   for j=1:(size(d_orgs,2)-1)
       if (divisionCycleState(j) > divisionCycleState(j+1))
           d_orgs(i,lastDivisionTimePoint:j,indices.relativeDivisionCycleState) = 0:(1/(j-lastDivisionTimePoint)):1.0;
           lastDivisionTimePoint = j+1;
       end           
   end
end

%% generate the image data
if (generateImages == true)
    timePoints = 1:size(d_orgs,2)%[125, 250, 375, 498];
    for i=generate_rowvector(timePoints)

        %% initialize the raw and the label image
        rawImageSize = round(settings.physicalImageSize ./ settings.outputImageSpacing);
        rawImage = zeros(generate_rowvector(rawImageSize(1:settings.imageDimension)));
        labelImage = zeros(generate_rowvector(rawImageSize(1:settings.imageDimension)));

        %% plot the objects
        numFeatures = 14; % id, volume, xpos, ypos, zpos, xsize, ysize, zsize, minIntensity, maxIntensity, meanIntensity, predecessorID, successorID1, successorID2
        numObjects = sum(d_orgs(:,i,1)>0);
        currentRegionProps = zeros(numObjects, numFeatures);
        for j=1:numObjects
            if (sum(d_orgs(j,i,indices.position)) > 0)

                %% id 2d images are desired put all objects on the z-center plane and remove z-direction
                if (settings.images2D == true)
                    d_orgs(j,i,indices.position(3)) = parameter.gui.benchmark.physicalImageSize(3) / 2;
                    d_orgs(j,i,indices.direction(3)) = 0;
                end

                %% extract necessary data for current object
                videoObject = d_orgs(j,i,indices.objectVideoId);
                videoTimePoint = round(d_orgs(j,i,indices.relativeDivisionCycleState)*(length(objectVideos{videoObject}.objects)-1))+1;
                object = objectVideos{videoObject}.objects{videoTimePoint};
                timepoint = videoTimePoint; %%% POTENTIAL ERROR LOCATION :-)

                %% convert the ground truth object space to output image space
                scalingFactor = 1.0;
                rawObjectSize = round(size(object.rawImage) .* generate_rowvector(settings.groundTruthSpacing(1:settings.imageDimension)) ./ generate_rowvector(settings.outputImageSpacing(1:settings.imageDimension)) );

                rawObjectSize(1) = scalingFactor * rawObjectSize(1);
                rawObjectSize(2) = scalingFactor * rawObjectSize(2);

                rawObjectImage = imresize3(object.rawImage, rawObjectSize);
                labelObjectImage = imresize3(object.groundTruthImage, rawObjectSize);

                %% add current object to raw and label image
                cellImage = rawObjectImage;
                cellLabelImage = j*(labelObjectImage>0);
                position = squeeze(d_orgs(j,i,indices.position))' ./ generate_rowvector(settings.outputImageSpacing);
                imageDimension = ndims(cellImage);
                topLeftCorner = round(position(1:imageDimension) - (size(cellImage) / 2));

                %% specify the ranges to copy, including a validity check if the region fits
                rangeX = max(1,topLeftCorner(1)):min((topLeftCorner(1)+size(cellImage,1)-1), size(rawImage,1));
                rangeY = max(1,topLeftCorner(2)):min((topLeftCorner(2)+size(cellImage,2)-1), size(rawImage,2));

                if (imageDimension == 2)
                    rawImage(rangeX,rangeY) = max(rawImage(rangeX, rangeY), cellImage(1:length(rangeX), 1:length(rangeY)));
                    labelImage(rangeX,rangeY) = max(labelImage(rangeX, rangeY), cellLabelImage(1:length(rangeX), 1:length(rangeY)));
                else
                    %% copy the object image to the full image
                    rangeZ = max(1,topLeftCorner(3)):min((topLeftCorner(3)+size(cellImage,3)-1), size(rawImage,3));
                    rawImage(rangeX,rangeY,rangeZ) = max(rawImage(rangeX, rangeY, rangeZ), cellImage(1:length(rangeX), 1:length(rangeY), 1:length(rangeZ)));
                    labelImage(rangeX,rangeY,rangeZ) = max(labelImage(rangeX, rangeY, rangeZ), cellLabelImage(1:length(rangeX), 1:length(rangeY), 1:length(rangeZ)));
                end

            end

            disp(['Finished processing ' num2str(j) ' / ' num2str(numObjects)]);
        end

        if (settings.maximumProjectionImages == true)
            rawImage = max(im2uint16(rawImage), [], 3);
            bfsave(rawImage, [pathName 'rawImage_max_t=' num2str(i) '.tif']);
        else
            bfsave(im2uint16(rawImage*65535), [pathName 'rawImage_max_t=' num2str(i) '.tif']);
        end
        

        if (settings.maximumProjectionImages == true)
            labelImage = max(im2uint16(labelImage/65535), [], 3);
            bfsave(labelImage, [pathName 'labelImage_max_t=' num2str(i) '.tif']);
        else
            bfsave(im2uint16(labelImage/65535), [pathName 'labelImage_max_t=' num2str(i) '.tif']);
        end
        
    end
end