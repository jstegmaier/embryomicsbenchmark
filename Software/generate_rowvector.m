function [ rowvector ] = generate_rowvector( vector )
%function [ rowvector ] = generate_rowvector( vector )
%returns the transposed vector if vector is a column vector and vector else


if iscolumn(vector)
   rowvector = vector';
else
   rowvector = vector;
end

