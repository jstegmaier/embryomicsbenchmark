function repulsiveForce = benchmark_calculate_repulsive_force(x, nucleusRadius, membraneRadius, M, m)
%function repulsiveForce = benchmark_calculate_repulsive_force(x, nucleusRadius, membraneRadius, M, m)
% Calculates the gradient of a repulsive force based on the publication by
% Macklin et al. 2012 "Patient-calibrated agent-based modelling of ductal carcinoma in situ (DCIS): From microscopic measurements to macroscopic predictions of clinical progression"
%
% This code is part of the MATLAB toolbox Gait-CAD.
% Copyright (C) 2013 [Johannes Stegmaier, Andreas Bartschat, Arif ul Maula Khan, Ralf Mikut]
%
%
% Last file change: 23-Sep-2013 09:30
%
% This program is free software; you can redistribute it and/or modify,
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General gaitPublic License for more details.
%
% You should have received a copy of the GNU General Public License along with this program;
% if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA.
%
% You will find further information about Gait-CAD in the manual or in the following conference paper:
%
% STEGMAIER,J.;ALSHUT,R.;REISCHL,M.;MIKUT,R.: Information Fusion of Image Analysis, Video Object Tracking, and Data Mining of Biological Images using the Open Source MATLAB Toolbox Gait-CAD.
% In:  Proc., DGBMT-Workshop Biosignal processing, Jena, 2012, pp. 109-111; 2012
% Online available: http://www.degruyter.com/view/j/bmte.2012.57.issue-s1-B/bmt-2012-4073/bmt-2012-4073.xml
%
% Please refer to this paper, if you use Gait-CAD with the ImVid extension for your scientific work.

x = generate_rowvector(x);

%% calculate the repulsive force
distance = zeros(size(x,1), 1);
for i=1:size(x,2)
    distance = distance + x(:,i).^2;
end
distance = sqrt(distance);
repulsiveForce = zeros(size(x));

nucleusRadius = generate_columnvector(nucleusRadius);
membraneRadius = generate_columnvector(membraneRadius);

%% if distance is smaller than the inner radius
validIndices1 = find(distance <= nucleusRadius);
c = generate_columnvector((ones(size(membraneRadius))-(nucleusRadius./membraneRadius)).^(m+1) - M);
factor = -(c.*(distance./nucleusRadius)+M)./distance;
newFactor = zeros(size(x));
for i=1:size(x,2)
    newFactor(:,i) = factor;
end
repulsiveForce(validIndices1,:) = newFactor(validIndices1,:) .* x(validIndices1,:);
    

%% if distance is between inner and outer radius
validIndices2 = find(distance > nucleusRadius & distance <= membraneRadius);
factor = -((ones(size(membraneRadius))-(distance./membraneRadius)).^(m+1))./distance;
newFactor = zeros(size(x));
for i=1:size(x,2)
    newFactor(:,i) = factor;
end
repulsiveForce(validIndices2,:) = newFactor(validIndices2,:) .* x(validIndices2,:);
